src = main.c git-tutorial-submodule/func.c
inc = git-tutorial-submodule

main: $(obj)
	$(CC) $(src) -I $(inc) -o $@ $^

.PHONY: clean
clean:
	rm -f $(obj) main
